package com.devcamp.province.api.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.*;

@Entity
@Table(name = "province")
public class CProvince {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int provinceId;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "provinces",cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<CDistrict> districts;


    public CProvince() {
        
    }

    public CProvince(int province_id, String code, String name, Set<CDistrict> districts) {
        this.provinceId = province_id;
        this.code = code;
        this.name = name;
        this.districts = districts;
    }

    public int getProvince_id() {
        return provinceId;
    }

    public void setProvince_id(int province_id) {
        this.provinceId = province_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CDistrict> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<CDistrict> districts) {
        this.districts = districts;
    }

}
