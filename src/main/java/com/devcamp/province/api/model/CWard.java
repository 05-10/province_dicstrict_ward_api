package com.devcamp.province.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.*;


@Entity
@Table(name = "ward")
public class CWard {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int wardId;

    @Column(name = "name")
    private String name;

    @Column(name = "prefix")
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "districtId")
    @JsonBackReference
    private CDistrict districts;

    public CWard() {
    }

    public CWard(int wardId, String name, String prefix, CDistrict district) {
        this.wardId = wardId;
        this.name = name;
        this.prefix = prefix;
        this.districts = district;
    }

    public int getId() {
        return wardId;
    }

    public void setId(int wardId) {
        this.wardId = wardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

}
