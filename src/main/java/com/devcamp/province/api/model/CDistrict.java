package com.devcamp.province.api.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.*;


@Entity
@Table(name = "district")
public class CDistrict {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int districtId;

    @Column(name = "name")
    private String name;

    @Column(name = "prefix")
    private String prefix;
    
    @ManyToOne
    @JoinColumn(name = "provinceId")
    @JsonBackReference
    private CProvince provinces;

    @OneToMany(mappedBy = "districts",cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<CWard> wards;

    public CDistrict() {

    }

    public CDistrict(int districtId, String name, String prefix, CProvince province, Set<CWard> wards) {
        this.districtId = districtId;
        this.name = name;
        this.prefix = prefix;
        this.provinces = province;
        this.wards = wards;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Set<CWard> getWards() {
        return wards;
    }

    public void setWards(Set<CWard> wards) {
        this.wards = wards;
    }

    
}
