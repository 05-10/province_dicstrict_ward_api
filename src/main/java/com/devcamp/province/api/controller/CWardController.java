package com.devcamp.province.api.controller;

import java.util.List;

import com.devcamp.province.api.model.CWard;
import com.devcamp.province.api.respository.CWardRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
@RestController
@RequestMapping("/")
@CrossOrigin
public class CWardController {
    
    @Autowired
    CWardRespository wardRespository;

    @GetMapping("/wards")
    public ResponseEntity<List<CWard>> getAllWards () {

        try {
            List<CWard> wards =  new ArrayList<>();

            wardRespository.findAll().forEach(wards::add);
            return new ResponseEntity<>(wards,HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
