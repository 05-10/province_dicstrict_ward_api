package com.devcamp.province.api.controller;

import com.devcamp.province.api.respository.CDistrictResponsitory;
import com.devcamp.province.api.respository.CProvinceRespository;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

import com.devcamp.province.api.model.CDistrict;
import com.devcamp.province.api.model.CWard;

import java.util.ArrayList;
@RestController
@CrossOrigin
@RequestMapping("/")
public class CDistrictController {
    
    @Autowired
    CDistrictResponsitory districtResponsitory;

    @GetMapping("/districts")
    public ResponseEntity<List<CDistrict>> getAllDistricts(){

        try {

            List<CDistrict> districts = new ArrayList<CDistrict>();
            districtResponsitory.findAll().forEach(districts::add);
            return new ResponseEntity<>(districts,HttpStatus.OK);

        }
        catch (Exception ex) {

            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping("/district")
    public ResponseEntity<Set<CWard>> getWardsByDicstrictId(@RequestParam(name = "id") int dicstrictId){

        try {
            CDistrict vDistrict = districtResponsitory.findByDistrictId(dicstrictId);

            if (vDistrict != null){
                return new ResponseEntity<>(vDistrict.getWards(),HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
