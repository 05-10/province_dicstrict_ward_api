package com.devcamp.province.api.controller;

import java.util.List;
import java.util.Set;

import com.devcamp.province.api.model.CDistrict;
import com.devcamp.province.api.model.CProvince;
import com.devcamp.province.api.respository.CProvinceRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CProvinceController {
    
    @Autowired
    CProvinceRespository provinceRespository;

    @GetMapping("/provinces")
    public ResponseEntity<List<CProvince>> getAllProvinces () {

        try {
            List<CProvince> provinces =  new ArrayList<>();

            provinceRespository.findAll().forEach(provinces::add);
            return new ResponseEntity<>(provinces,HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/province")
    public ResponseEntity<Set<CDistrict>> getDistrictsByProvinceId(@RequestParam(name = "id") int provinceId){

        try {
            CProvince vProvince = provinceRespository.findByProvinceId(provinceId);

            if (vProvince != null){
                return new ResponseEntity<>(vProvince.getDistricts(),HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
}
