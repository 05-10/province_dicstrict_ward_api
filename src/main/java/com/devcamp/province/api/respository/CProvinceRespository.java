package com.devcamp.province.api.respository;

import com.devcamp.province.api.model.CProvince;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CProvinceRespository extends JpaRepository<CProvince,Integer>{
    
    CProvince findByProvinceId(Integer provinceId);
    
}
