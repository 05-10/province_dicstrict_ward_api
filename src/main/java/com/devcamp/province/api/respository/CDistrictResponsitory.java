package com.devcamp.province.api.respository;

import com.devcamp.province.api.model.CDistrict;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CDistrictResponsitory extends JpaRepository<CDistrict,Integer> {
    
    CDistrict findByDistrictId(Integer districtId);
    
}
