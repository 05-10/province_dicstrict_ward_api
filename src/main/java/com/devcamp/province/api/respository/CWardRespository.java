package com.devcamp.province.api.respository;

import com.devcamp.province.api.model.CWard;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CWardRespository extends JpaRepository<CWard,Integer>{
    
}
